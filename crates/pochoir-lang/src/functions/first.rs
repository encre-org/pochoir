//! Returns the first element of an array.
//!
//! ### Example
//!
//! <div class="example-wrap"><pre class="rust rust-example-rendered" style="border-left: 2px solid red;"><span style="position: absolute; right: 0; top: 0; padding: 0.1rem 0.4rem 0 0; font-size: 0.75em; font-weight: bold; color: #333;">input</span><code><span class="fn">first</span>([<span class="string">"a"</span>, <span class="string">"b"</span>, <span class="string">"c"</span>])</code></pre></div>
//!
//! <div class="example-wrap"><pre class="rust rust-example-rendered" style="border-left: 2px solid red;"><span style="position: absolute; right: 0; top: 0; padding: 0.1rem 0.4rem 0 0; font-size: 0.75em; font-weight: bold; color: #333;">output</span><code><span class="string">"a"</span></code></pre></div>
//!
//! <div class="example-wrap"><pre class="rust rust-example-rendered" style="position: relative; margin-top: 2rem; border-left: 2px solid blue;"><span style="position: absolute; right: 0; top: 0; padding: 0.1rem 0.4rem 0 0; font-size: 0.75em; font-weight: bold; color: #333;">input</span><code><span class="fn">first</span>([])</code></pre></div>
//!
//! <div class="example-wrap"><pre class="rust rust-example-rendered" style="border-left: 2px solid blue;"><span style="position: absolute; right: 0; top: 0; padding: 0.1rem 0.4rem 0 0; font-size: 0.75em; font-weight: bold; color: #333;">output</span><code>null</code></pre></div>
use crate::{FunctionResult, Value};

pub(crate) fn first(val: Vec<Value>) -> FunctionResult<Value> {
    Ok(val.into_iter().next().unwrap_or(Value::Null))
}

#[cfg(test)]
mod tests {
    use crate::IntoValue;

    use super::*;

    #[test]
    fn first_test() {
        assert_eq!(
            first(vec!["a".into_value(), "b".into_value(), "c".into_value()]).unwrap(),
            "a".into_value(),
        );

        assert_eq!(first(vec![]).unwrap(), Value::Null,);
    }
}
