//! Takes an array of values and returns an array of array of values with their indices.
//!
//! **The first index is 1.**
//!
//! A second (optional) argument can be used to change the first index.
//!
//! ### Example
//!
//! <div class="example-wrap"><pre class="rust rust-example-rendered" style="border-left: 2px solid red;"><span style="position: absolute; right: 0; top: 0; padding: 0.1rem 0.4rem 0 0; font-size: 0.75em; font-weight: bold; color: #333;">input</span><code><span class="fn">enumerate</span>([<span class="string">"a"</span>, <span class="string">"b"</span>, <span class="string">"c"</span>])</code></pre></div>
//!
//! <div class="example-wrap"><pre class="rust rust-example-rendered" style="border-left: 2px solid red;"><span style="position: absolute; right: 0; top: 0; padding: 0.1rem 0.4rem 0 0; font-size: 0.75em; font-weight: bold; color: #333;">output</span><code>[[<span class="number">1</span>, <span class="string">"a"</span>], [<span class="number">2</span>, <span class="string">"b"</span>], [<span class="number">3</span>, <span class="string">"c"</span>]]</code></pre></div>
//!
//! <div class="example-wrap"><pre class="rust rust-example-rendered" style="position: relative; margin-top: 2rem; border-left: 2px solid blue;"><span style="position: absolute; right: 0; top: 0; padding: 0.1rem 0.4rem 0 0; font-size: 0.75em; font-weight: bold; color: #333;">input</span><code><span class="fn">enumerate</span>([<span class="string">"a"</span>, <span class="string">"b"</span>, <span class="string">"c"</span>], <span class="number">0</span>)</code></pre></div>
//!
//! <div class="example-wrap"><pre class="rust rust-example-rendered" style="border-left: 2px solid blue;"><span style="position: absolute; right: 0; top: 0; padding: 0.1rem 0.4rem 0 0; font-size: 0.75em; font-weight: bold; color: #333;">output</span><code>[[<span class="number">0</span>, <span class="string">"a"</span>], [<span class="number">1</span>, <span class="string">"b"</span>], [<span class="number">2</span>, <span class="string">"c"</span>]]</code></pre></div>
use crate::{value::IntoValue, FunctionResult, Value};

const DEFAULT_START_INDEX: usize = 1;

pub(crate) fn enumerate(val: Vec<Value>, start_index: Option<usize>) -> FunctionResult<Vec<Value>> {
    Ok(val
        .into_iter()
        .enumerate()
        .map(|(index, val)| {
            Value::Array(vec![
                (start_index.unwrap_or(DEFAULT_START_INDEX) + index).into_value(),
                val,
            ])
        })
        .collect())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn enumerate_test() {
        assert_eq!(
            enumerate(vec!["".into_value(), 42.into_value()], None).unwrap(),
            vec![
                vec![Value::Number(1.0), "".into_value()].into_value(),
                vec![Value::Number(2.0), 42.into_value()].into_value()
            ],
        );

        assert_eq!(
            enumerate(vec!["".into_value(), 42.into_value()], Some(0)).unwrap(),
            vec![
                vec![Value::Number(0.0), "".into_value()].into_value(),
                vec![Value::Number(1.0), 42.into_value()].into_value()
            ],
        );
    }
}
