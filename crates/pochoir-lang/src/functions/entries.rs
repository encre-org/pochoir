//! Takes an object and returns an array of array of values with their keys.
//!
//! # Example
//!
//! <div class="example-wrap"><pre class="rust rust-example-rendered"><span style="position: absolute; right: 0; top: 0; padding: 0.1rem 0.4rem 0 0; font-size: 0.75em; font-weight: bold; color: #333;">input</span><code><span class="fn">entries</span>({ a: <span class="number">1</span>, b: <span class="number">2</span>, c: <span class="string">"hello"</span> })</code></pre></div>
//!
//! <div class="example-wrap"><pre class="rust rust-example-rendered"><span style="position: absolute; right: 0; top: 0; padding: 0.1rem 0.4rem 0 0; font-size: 0.75em; font-weight: bold; color: #333;">output</span><code>[[<span class="string">"a"</span>, <span class="number">1</span>], [<span class="string">"b"</span>, <span class="number">2</span>], [<span class="string">"c"</span>, <span class="string">"hello"</span>]]</code></pre></div>
use crate::{FunctionResult, Object, Value};

pub(crate) fn entries(val: Object) -> FunctionResult<Vec<(String, Value)>> {
    Ok(val.into_iter().collect())
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::{object, value::IntoValue};

    #[test]
    fn entries_test() {
        assert_eq!(
            entries(object! {
                "a" => 1.into_value(),
                "b" => 2.into_value(),
                "c" => "hello".into_value(),
            })
            .unwrap(),
            vec![
                ("a".into(), 1.into_value()),
                ("b".into(), 2.into_value()),
                ("c".into(), "hello".into_value()),
            ]
        );
    }
}
