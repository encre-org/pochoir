//! Replace all occurrences of a sub-string in a string.
//!
//! ### Example
//!
//! <div class="example-wrap"><pre class="rust rust-example-rendered" style="border-left: 2px solid red;"><span style="position: absolute; right: 0; top: 0; padding: 0.1rem 0.4rem 0 0; font-size: 0.75em; font-weight: bold; color: #333;">input</span><code><span class="fn">replace</span>(<span class="string">"The quick brown fox jumps over the lazy dog."</span>, <span class="string">"quick"</span>, <span class="string">"speedy"</span>)</code></pre></div>
//!
//! <div class="example-wrap"><pre class="rust rust-example-rendered" style="border-left: 2px solid red;"><span style="position: absolute; right: 0; top: 0; padding: 0.1rem 0.4rem 0 0; font-size: 0.75em; font-weight: bold; color: #333;">output</span><code><span class="string">"The speedy brown fox jumps over the lazy dog."</span></code></pre></div>
use crate::FunctionResult;

pub(crate) fn replace(val: String, replace: String, with: String) -> FunctionResult<String> {
    Ok(val.replace(&replace, &with))
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn replace_test() {
        assert_eq!(
            replace(
                "The quick brown fox jumps over the lazy dog.".to_string(),
                "quick".to_string(),
                "speedy".to_string(),
            )
            .unwrap(),
            "The speedy brown fox jumps over the lazy dog."
        );
    }
}
