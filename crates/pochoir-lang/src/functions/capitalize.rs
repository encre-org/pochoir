//! Capitalize a word by turning its first character into uppercase.
//!
//! It supports a large range of Unicode characters, for example it turns "é" into "É" and  "ß" into "SS".
//!
//! ### Example
//!
//! <div class="example-wrap"><pre class="rust rust-example-rendered" style="border-left: 2px solid red;"><span style="position: absolute; right: 0; top: 0; padding: 0.1rem 0.4rem 0 0; font-size: 0.75em; font-weight: bold; color: #333;">input</span><code><span class="fn">capitalize</span>(<span class="string">"hello"</span>)</code></pre></div>
//!
//! <div class="example-wrap"><pre class="rust rust-example-rendered" style="border-left: 2px solid red;"><span style="position: absolute; right: 0; top: 0; padding: 0.1rem 0.4rem 0 0; font-size: 0.75em; font-weight: bold; color: #333;">output</span><code><span class="string">"Hello"</span></code></pre></div>
//!
//! <div class="example-wrap"><pre class="rust rust-example-rendered" style="border-left: 2px solid blue;"><span style="position: absolute; right: 0; top: 0; padding: 0.1rem 0.4rem 0 0; font-size: 0.75em; font-weight: bold; color: #333;">input</span><code><span class="fn">capitalize</span>(<span class="string">"édouard"</span>)</code></pre></div>
//!
//! <div class="example-wrap"><pre class="rust rust-example-rendered" style="border-left: 2px solid blue;"><span style="position: absolute; right: 0; top: 0; padding: 0.1rem 0.4rem 0 0; font-size: 0.75em; font-weight: bold; color: #333;">output</span><code><span class="string">"Édouard"</span></code></pre></div>
use crate::FunctionResult;

pub(crate) fn capitalize(val: String) -> FunctionResult<String> {
    let mut chars = val.chars();

    Ok(if let Some(first_char) = chars.next() {
        first_char.to_uppercase().chain(chars).collect()
    } else {
        String::new()
    })
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn capitalize_test() {
        assert_eq!(
            capitalize("hello".to_string()).unwrap(),
            "Hello".to_string()
        );
        assert_eq!(
            capitalize("\u{e9}douard".to_string()).unwrap(),
            "\u{c9}douard".to_string()
        );
    }
}
