//! Compute the average time (in minutes, rounded up to the next integer) to read a piece of text (using 183 Words Per Minute according to <https://thereadtime.com>).
//!
//! ### Example
//!
//! <div class="example-wrap"><pre class="rust rust-example-rendered"><span style="position: absolute; right: 0; top: 0; padding: 0.1rem 0.4rem 0 0; font-size: 0.75em; font-weight: bold; color: #333;">input</span><code><span class="fn">reading_time</span>(<span class="string">"foo "</span> * <span class="number">183</span> * <span class="number">12</span>)</code></pre></div>
//!
//! <div class="example-wrap"><pre class="rust rust-example-rendered"><span style="position: absolute; right: 0; top: 0; padding: 0.1rem 0.4rem 0 0; font-size: 0.75em; font-weight: bold; color: #333;">output</span><code><span class="number">12</span></code></pre></div>
use crate::FunctionResult;

pub(crate) fn reading_time(val: String) -> FunctionResult<f32> {
    #[allow(
        clippy::cast_possible_truncation,
        clippy::cast_precision_loss,
        clippy::cast_sign_loss
    )]
    Ok(val.split_whitespace().count() as f32 / 183.)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn reading_time_test() {
        assert!((reading_time("foo ".repeat(183 * 12)).unwrap() - 12.0).abs() < f32::EPSILON);
    }
}
