//! Count the number of words in a piece of text.
//!
//! ### Example
//!
//! <div class="example-wrap"><pre class="rust rust-example-rendered" style="border-left: 2px solid red;"><span style="position: absolute; right: 0; top: 0; padding: 0.1rem 0.4rem 0 0; font-size: 0.75em; font-weight: bold; color: #333;">input</span><code><span class="fn">word_count</span>(<span class="string">"What will you do with pochoir?"</span>)</code></pre></div>
//!
//! <div class="example-wrap"><pre class="rust rust-example-rendered" style="border-left: 2px solid red;"><span style="position: absolute; right: 0; top: 0; padding: 0.1rem 0.4rem 0 0; font-size: 0.75em; font-weight: bold; color: #333;">output</span><code><span class="number">6</span></code></pre></div>
//!
//! <div class="example-wrap"><pre class="rust rust-example-rendered" style="position: relative; margin-top: 2rem; border-left: 2px solid blue;"><span style="position: absolute; right: 0; top: 0; padding: 0.1rem 0.4rem 0 0; font-size: 0.75em; font-weight: bold; color: #333;">input</span><code><span class="fn">word_count</span>(<span class="string">"A message with extra   spaces and a number: 42"</span>)</code></pre></div>
//!
//! <div class="example-wrap"><pre class="rust rust-example-rendered" style="border-left: 2px solid blue;"><span style="position: absolute; right: 0; top: 0; padding: 0.1rem 0.4rem 0 0; font-size: 0.75em; font-weight: bold; color: #333;">output</span><code><span class="number">9</span></code></pre></div>
use crate::FunctionResult;

pub(crate) fn word_count(val: String) -> FunctionResult<usize> {
    Ok(val
        .split_whitespace()
        .filter(|w| !w.is_empty() && !w.chars().all(|ch| !ch.is_alphanumeric()))
        .count())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn word_count_test() {
        assert_eq!(word_count("hello     world".into()).unwrap(), 2,);
        assert_eq!(word_count("bonjour le monde !".into()).unwrap(), 3,);
        assert_eq!(
            word_count("A message with extra spaces and a number: 42".into()).unwrap(),
            9,
        );
    }
}
