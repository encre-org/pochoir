//! Round a number to the nearest integer.
//!
//! ### Example
//!
//! <div class="example-wrap"><pre class="rust rust-example-rendered" style="border-left: 2px solid red;"><span style="position: absolute; right: 0; top: 0; padding: 0.1rem 0.4rem 0 0; font-size: 0.75em; font-weight: bold; color: #333;">input</span><code><span class="fn">round</span>(<span class="number">22.33</span>)</code></pre></div>
//!
//! <div class="example-wrap"><pre class="rust rust-example-rendered" style="border-left: 2px solid red;"><span style="position: absolute; right: 0; top: 0; padding: 0.1rem 0.4rem 0 0; font-size: 0.75em; font-weight: bold; color: #333;">output</span><code><span class="number">22</span></code></pre></div>
//!
//! <div class="example-wrap"><pre class="rust rust-example-rendered" style="position: relative; margin-top: 2rem; border-left: 2px solid blue;"><span style="position: absolute; right: 0; top: 0; padding: 0.1rem 0.4rem 0 0; font-size: 0.75em; font-weight: bold; color: #333;">input</span><code><span class="fn">round</span>(<span class="number">22.5</span>)</code></pre></div>
//!
//! <div class="example-wrap"><pre class="rust rust-example-rendered" style="border-left: 2px solid blue;"><span style="position: absolute; right: 0; top: 0; padding: 0.1rem 0.4rem 0 0; font-size: 0.75em; font-weight: bold; color: #333;">output</span><code><span class="number">23</span></code></pre></div>
use crate::FunctionResult;

pub(crate) fn round(num: f32) -> FunctionResult<f32> {
    Ok(num.round())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn round_test() {
        assert!((round(22.33).unwrap() - 22.0).abs() < f32::EPSILON);
        assert!((round(22.5).unwrap() - 23.0).abs() < f32::EPSILON);
    }
}
