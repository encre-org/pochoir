use pochoir_common::Spanned;
use std::ops::Range;

use super::parser::Token;
use crate::{
    parser::{BinaryOp, Literal, TernaryOp, UnaryOp},
    Context, Error, Object, Result, Value,
};

fn span_of_tokens(tokens: &[Spanned<Token>]) -> Range<usize> {
    if tokens.is_empty() {
        0..0
    } else {
        tokens.first().unwrap().span().start..tokens.last().unwrap().span().end
    }
}

pub(crate) fn interpret(mut tokens: Vec<Spanned<Token>>, context: &mut Context) -> Result<Value> {
    tokens.reverse();

    interpret_recursive(&mut tokens, context, None)
}

#[allow(
    clippy::too_many_lines,
    clippy::cast_possible_truncation,
    clippy::cast_sign_loss
)]
pub(crate) fn interpret_recursive(
    tokens: &mut Vec<Spanned<Token>>,
    context: &mut Context,
    mut pipe_result: Option<Value>,
) -> Result<Value> {
    let mut result = Value::Null;

    while let Some(token) = tokens.pop() {
        let file_path = token.file_path().to_owned();
        let span = token.span().clone();

        match token.into_inner() {
            Token::UnaryOperator(op, mut val) => {
                let span = span_of_tokens(&val);
                let interpreted_val = interpret_recursive(&mut val, context, None)?;

                match op {
                    UnaryOp::Negate => {
                        if let Value::Number(num) = interpreted_val {
                            result = Value::Number(-num);
                        } else {
                            return Err(Spanned::new(Error::MismatchedTypes {
                                expected: "Number".to_string(),
                                found: interpreted_val.type_name().to_string(),
                            })
                            .with_span(span)
                            .with_file_path(file_path));
                        }
                    }
                    UnaryOp::BoolNot => {
                        if let Value::Bool(boolean) = interpreted_val {
                            result = Value::Bool(!boolean);
                        } else {
                            return Err(Spanned::new(Error::MismatchedTypes {
                                expected: "Bool".to_string(),
                                found: interpreted_val.type_name().to_string(),
                            })
                            .with_span(span)
                            .with_file_path(file_path));
                        }
                    }
                    UnaryOp::Range | UnaryOp::RangeInclusive => {
                        let range_end = if interpreted_val == Value::Null {
                            None
                        } else if let Value::Number(num) = interpreted_val {
                            if num.fract() == 0.0 {
                                Some(num as i32 + i32::from(op == UnaryOp::RangeInclusive))
                            } else {
                                return Err(Spanned::new(Error::BadRangeBound(
                                    "a decimal number".to_string(),
                                ))
                                .with_span(span)
                                .with_file_path(file_path));
                            }
                        } else {
                            return Err(Spanned::new(Error::MismatchedTypes {
                                expected: "Number".to_string(),
                                found: interpreted_val.type_name().to_string(),
                            })
                            .with_span(span)
                            .with_file_path(file_path));
                        };

                        result = Value::Range(None, range_end);
                    }
                }
            }
            Token::BinaryOperator(op, mut left, mut right) => {
                if op == BinaryOp::Pipe {
                    let left_value = interpret_recursive(&mut left, context, None)?;
                    let right_value = interpret_recursive(&mut right, context, Some(left_value))?;
                    result = right_value;

                    continue;
                } else if op == BinaryOp::Definition {
                    if !left.is_empty() && left.len() < 2 {
                        if let Token::Variable(name) = &*left[0] {
                            let right_value = interpret_recursive(&mut right, context, None)?;
                            context.insert(&**name, right_value.clone());
                            result = right_value;
                        } else {
                            return Err(Spanned::new(Error::InvalidLeftHandDefinition)
                                .with_span(span_of_tokens(&left))
                                .with_file_path(file_path));
                        }
                    } else {
                        return Err(Spanned::new(Error::InvalidLeftHandDefinition)
                            .with_span(span_of_tokens(&left))
                            .with_file_path(file_path));
                    }

                    continue;
                }

                let left_span = span_of_tokens(&left);
                let left_value = interpret_recursive(&mut left, context, None)?;

                // Lazy operators might not need the right value evaluated
                if op == BinaryOp::BoolOr && left_value == Value::Bool(true) {
                    result = Value::Bool(true);
                    continue;
                } else if op == BinaryOp::BoolAnd && left_value == Value::Bool(false) {
                    result = Value::Bool(false);
                    continue;
                }

                let right_span = span_of_tokens(&right);
                let right_value = interpret_recursive(&mut right, context, None)?;

                match op {
                    BinaryOp::Add => match (left_value, right_value) {
                        (Value::Number(lhs_num), Value::Number(rhs_num)) => {
                            result = Value::Number(lhs_num + rhs_num);
                        }
                        (Value::String(lhs_str), Value::String(rhs_str)) => {
                            result = Value::String(lhs_str + &rhs_str);
                        }
                        (left_value, right_value) => {
                            return Err(Spanned::new(Error::MismatchedTypes {
                                expected: "Number + Number or String + String".to_string(),
                                found: format!(
                                    "{} + {}",
                                    left_value.type_name(),
                                    right_value.type_name(),
                                ),
                            })
                            .with_span(span)
                            .with_file_path(file_path))
                        }
                    },
                    BinaryOp::Subtract => match (left_value, right_value) {
                        (Value::Number(lhs_num), Value::Number(rhs_num)) => {
                            result = Value::Number(lhs_num - rhs_num);
                        }
                        (Value::Number(_), right_value) => {
                            return Err(Spanned::new(Error::MismatchedTypes {
                                expected: "Number".to_string(),
                                found: right_value.type_name().to_string(),
                            })
                            .with_span(right_span)
                            .with_file_path(file_path))
                        }
                        (left_value, _) => {
                            return Err(Spanned::new(Error::MismatchedTypes {
                                expected: "Number".to_string(),
                                found: left_value.type_name().to_string(),
                            })
                            .with_span(left_span)
                            .with_file_path(file_path))
                        }
                    },
                    BinaryOp::Multiply => match (left_value, right_value) {
                        (Value::Number(lhs_num), Value::Number(rhs_num)) => {
                            result = Value::Number(lhs_num * rhs_num);
                        }
                        (Value::String(lhs_string), Value::Number(rhs_num)) => {
                            if rhs_num.is_sign_positive() && rhs_num.fract() == 0.0 {
                                result = Value::String(lhs_string.repeat(rhs_num as usize));
                            } else {
                                return Err(Spanned::new(Error::MismatchedTypes {
                                    expected:
                                        "Number * Number or String * Number or Number * String"
                                            .to_string(),
                                    found: "String * decimal number".to_string(),
                                })
                                .with_span(span)
                                .with_file_path(file_path));
                            }
                        }
                        (Value::Number(lhs_num), Value::String(rhs_string)) => {
                            if lhs_num.is_sign_positive() && lhs_num.fract() == 0.0 {
                                result = Value::String(rhs_string.repeat(lhs_num as usize));
                            } else {
                                return Err(Spanned::new(Error::MismatchedTypes {
                                    expected:
                                        "Number * Number or String * Number or Number * String"
                                            .to_string(),
                                    found: "decimal number * String".to_string(),
                                })
                                .with_span(span)
                                .with_file_path(file_path));
                            }
                        }
                        (left_value, right_value) => {
                            return Err(Spanned::new(Error::MismatchedTypes {
                                expected: "Number * Number or String * Number or Number * String"
                                    .to_string(),
                                found: format!(
                                    "{} * {}",
                                    left_value.type_name(),
                                    right_value.type_name(),
                                ),
                            })
                            .with_span(span)
                            .with_file_path(file_path))
                        }
                    },
                    BinaryOp::Divide => match (left_value, right_value) {
                        (Value::Number(lhs_num), Value::Number(rhs_num)) => {
                            result = Value::Number(lhs_num / rhs_num);
                        }
                        (Value::Number(_), right_value) => {
                            return Err(Spanned::new(Error::MismatchedTypes {
                                expected: "Number".to_string(),
                                found: right_value.type_name().to_string(),
                            })
                            .with_span(right_span)
                            .with_file_path(file_path))
                        }
                        (left_value, _) => {
                            return Err(Spanned::new(Error::MismatchedTypes {
                                expected: "Number".to_string(),
                                found: left_value.type_name().to_string(),
                            })
                            .with_span(left_span)
                            .with_file_path(file_path))
                        }
                    },
                    BinaryOp::Equal => {
                        let left_type = left_value.type_name();
                        let right_type = right_value.type_name();

                        if left_type == right_type
                            || left_value == Value::Null
                            || right_value == Value::Null
                        {
                            result = Value::Bool(left_value == right_value);
                        } else {
                            return Err(Spanned::new(Error::MismatchedTypes {
                                expected: left_type.to_string(),
                                found: right_type.to_string(),
                            })
                            .with_span(span)
                            .with_file_path(file_path));
                        }
                    }
                    BinaryOp::NotEqual => {
                        let left_type = left_value.type_name();
                        let right_type = right_value.type_name();

                        if left_type == right_type
                            || left_value == Value::Null
                            || right_value == Value::Null
                        {
                            result = Value::Bool(left_value != right_value);
                        } else {
                            return Err(Spanned::new(Error::MismatchedTypes {
                                expected: left_type.to_string(),
                                found: right_type.to_string(),
                            })
                            .with_span(span)
                            .with_file_path(file_path));
                        }
                    }
                    BinaryOp::Greater => match (left_value, right_value) {
                        (Value::Number(lhs_num), Value::Number(rhs_num)) => {
                            result = Value::Bool(lhs_num > rhs_num);
                        }
                        (Value::Number(_), right_value) => {
                            return Err(Spanned::new(Error::MismatchedTypes {
                                expected: "Number".to_string(),
                                found: right_value.type_name().to_string(),
                            })
                            .with_span(right_span)
                            .with_file_path(file_path))
                        }
                        (left_value, _) => {
                            return Err(Spanned::new(Error::MismatchedTypes {
                                expected: "Number".to_string(),
                                found: left_value.type_name().to_string(),
                            })
                            .with_span(left_span)
                            .with_file_path(file_path))
                        }
                    },
                    BinaryOp::GreaterOrEqual => match (left_value, right_value) {
                        (Value::Number(lhs_num), Value::Number(rhs_num)) => {
                            result = Value::Bool(lhs_num >= rhs_num);
                        }
                        (Value::Number(_), right_value) => {
                            return Err(Spanned::new(Error::MismatchedTypes {
                                expected: "Number".to_string(),
                                found: right_value.type_name().to_string(),
                            })
                            .with_span(right_span)
                            .with_file_path(file_path))
                        }
                        (left_value, _) => {
                            return Err(Spanned::new(Error::MismatchedTypes {
                                expected: "Number".to_string(),
                                found: left_value.type_name().to_string(),
                            })
                            .with_span(left_span)
                            .with_file_path(file_path))
                        }
                    },
                    BinaryOp::Less => match (left_value, right_value) {
                        (Value::Number(lhs_num), Value::Number(rhs_num)) => {
                            result = Value::Bool(lhs_num < rhs_num);
                        }
                        (Value::Number(_), right_value) => {
                            return Err(Spanned::new(Error::MismatchedTypes {
                                expected: "Number".to_string(),
                                found: right_value.type_name().to_string(),
                            })
                            .with_span(right_span)
                            .with_file_path(file_path))
                        }
                        (left_value, _) => {
                            return Err(Spanned::new(Error::MismatchedTypes {
                                expected: "Number".to_string(),
                                found: left_value.type_name().to_string(),
                            })
                            .with_span(left_span)
                            .with_file_path(file_path))
                        }
                    },
                    BinaryOp::LessOrEqual => match (left_value, right_value) {
                        (Value::Number(lhs_num), Value::Number(rhs_num)) => {
                            result = Value::Bool(lhs_num <= rhs_num);
                        }
                        (Value::Number(_), right_value) => {
                            return Err(Spanned::new(Error::MismatchedTypes {
                                expected: "Number".to_string(),
                                found: right_value.type_name().to_string(),
                            })
                            .with_span(right_span)
                            .with_file_path(file_path))
                        }
                        (left_value, _) => {
                            return Err(Spanned::new(Error::MismatchedTypes {
                                expected: "Number".to_string(),
                                found: left_value.type_name().to_string(),
                            })
                            .with_span(left_span)
                            .with_file_path(file_path))
                        }
                    },
                    BinaryOp::BoolAnd => match (left_value, right_value) {
                        (Value::Bool(lhs_bool), Value::Bool(rhs_bool)) => {
                            result = Value::Bool(lhs_bool && rhs_bool);
                        }
                        (Value::Bool(_), right_value) => {
                            return Err(Spanned::new(Error::MismatchedTypes {
                                expected: "Bool".to_string(),
                                found: right_value.type_name().to_string(),
                            })
                            .with_span(right_span)
                            .with_file_path(file_path))
                        }
                        (left_value, _) => {
                            return Err(Spanned::new(Error::MismatchedTypes {
                                expected: "Bool".to_string(),
                                found: left_value.type_name().to_string(),
                            })
                            .with_span(left_span)
                            .with_file_path(file_path))
                        }
                    },
                    BinaryOp::BoolOr => match (left_value, right_value) {
                        (Value::Bool(lhs_bool), Value::Bool(rhs_bool)) => {
                            result = Value::Bool(lhs_bool || rhs_bool);
                        }
                        (Value::Bool(_), right_value) => {
                            return Err(Spanned::new(Error::MismatchedTypes {
                                expected: "Bool".to_string(),
                                found: right_value.type_name().to_string(),
                            })
                            .with_span(right_span)
                            .with_file_path(file_path))
                        }
                        (left_value, _) => {
                            return Err(Spanned::new(Error::MismatchedTypes {
                                expected: "Bool".to_string(),
                                found: left_value.type_name().to_string(),
                            })
                            .with_span(left_span)
                            .with_file_path(file_path))
                        }
                    },
                    BinaryOp::Pipe | BinaryOp::Definition => unreachable!(),
                    BinaryOp::Semicolon => {
                        result = right_value;
                    }
                    BinaryOp::Range | BinaryOp::RangeInclusive => {
                        let range_start = if left_value == Value::Null {
                            None
                        } else if let Value::Number(num) = left_value {
                            if num.fract() == 0.0 {
                                Some(num as i32)
                            } else {
                                return Err(Spanned::new(Error::BadRangeBound(
                                    "a decimal number".to_string(),
                                ))
                                .with_span(left_span)
                                .with_file_path(file_path));
                            }
                        } else {
                            return Err(Spanned::new(Error::MismatchedTypes {
                                expected: "Number".to_string(),
                                found: left_value.type_name().to_string(),
                            })
                            .with_span(left_span)
                            .with_file_path(file_path));
                        };

                        let range_end = if right_value == Value::Null {
                            None
                        } else if let Value::Number(num) = right_value {
                            if num.fract() == 0.0 {
                                Some(num as i32 + i32::from(op == BinaryOp::RangeInclusive))
                            } else {
                                return Err(Spanned::new(Error::BadRangeBound(
                                    "a decimal number".to_string(),
                                ))
                                .with_span(right_span)
                                .with_file_path(file_path));
                            }
                        } else {
                            return Err(Spanned::new(Error::MismatchedTypes {
                                expected: "Number".to_string(),
                                found: left_value.type_name().to_string(),
                            })
                            .with_span(right_span)
                            .with_file_path(file_path));
                        };

                        result = Value::Range(range_start, range_end);
                    }
                }
            }
            Token::TernaryOperator(op, mut left, mut middle, mut right) => match op {
                TernaryOp::Conditional => {
                    let left_span = span_of_tokens(&left);
                    let left_value = interpret_recursive(&mut left, context, None)?;

                    if let Value::Bool(left_value) = left_value {
                        if left_value {
                            result = interpret_recursive(&mut middle, context, None)?;
                        } else {
                            result = interpret_recursive(&mut right, context, None)?;
                        }
                    } else {
                        return Err(Spanned::new(Error::MismatchedTypes {
                            expected: "Bool".to_string(),
                            found: left_value.type_name().to_string(),
                        })
                        .with_span(left_span)
                        .with_file_path(file_path));
                    }
                }
            },
            Token::Literal(literal) => match literal {
                Literal::String(val) => result = Value::String(val),
                Literal::Number(val) => result = Value::Number(val),
                Literal::Array(val) => {
                    result = Value::Array(
                        val.into_iter()
                            .map(|mut v| interpret_recursive(&mut v, context, None))
                            .collect::<Result<Vec<Value>>>()?,
                    );
                }
                Literal::Object(val) => {
                    result = Value::Object(
                        val.into_iter()
                            .map(|(k, mut v)| {
                                interpret_recursive(&mut v, context, None).map(|v| (k, v))
                            })
                            .collect::<Result<Object>>()?,
                    );
                }
                Literal::Bool(val) => result = Value::Bool(val),
                Literal::Null => result = Value::Null,
            },
            Token::FunctionCall(name_token, args) => {
                let name_span = name_token.span().clone();
                let function = interpret_recursive(&mut vec![*name_token], context, None)?;

                if let Value::Function(func) = function {
                    let args_span = if args.is_empty()
                        || args.first().unwrap().is_empty()
                        || args.last().unwrap().is_empty()
                    {
                        name_span
                    } else {
                        args.first().unwrap().first().unwrap().span().start
                            ..args.last().unwrap().last().unwrap().span().end
                    };
                    let args_iter = args
                        .into_iter()
                        .map(|mut arg| interpret_recursive(&mut arg, context, None));

                    let args_iter = if let Some(pipe_result) = pipe_result.take() {
                        std::iter::once(Ok(pipe_result))
                            .chain(args_iter)
                            .collect::<Result<Vec<Value>>>()?
                    } else {
                        args_iter.collect::<Result<Vec<Value>>>()?
                    };

                    match func.call(args_iter) {
                        Ok(new_result) => result = new_result,
                        Err(e) => {
                            return Err(Spanned::new(Error::FunctionError(e.to_string()))
                                .with_span(args_span)
                                .with_file_path(file_path))
                        }
                    }
                } else if function == Value::Null {
                    return Err(Spanned::new(Error::CannotCallNull)
                        .with_span(name_span)
                        .with_file_path(file_path));
                } else {
                    return Err(Spanned::new(Error::MismatchedTypes {
                        expected: "Function".to_string(),
                        found: function.type_name().to_string(),
                    })
                    .with_span(name_span)
                    .with_file_path(file_path));
                }
            }
            Token::Variable(name) => {
                result = match context.get(&*name) {
                    Some(result) => result.clone(),
                    None => Value::Null,
                };
            }
            Token::Index(mut index_key_tokens, parent_token) => {
                let index_span = span_of_tokens(&index_key_tokens);
                let index_key = interpret_recursive(&mut index_key_tokens, context, None)?;
                let parent_span = parent_token.span().clone();
                let parent_value = interpret_recursive(&mut vec![*parent_token], context, None)?;

                if let Value::Array(mut parent_value) = parent_value {
                    if let Value::Number(index_key) = index_key {
                        if index_key.is_sign_positive() && index_key.fract() == 0.0 {
                            result = if (index_key as usize) < parent_value.len() {
                                // It is useful to use remove here to avoid cloning the value
                                // because we are sure that if the value comes from the Context it
                                // was already cloned before
                                parent_value.swap_remove(index_key as usize)
                            } else {
                                Value::Null
                            };
                        } else {
                            return Err(Spanned::new(Error::BadArrayIndex(
                                "a decimal number".to_string(),
                            ))
                            .with_span(index_span)
                            .with_file_path(file_path));
                        }
                    } else if let Value::Range(start, end) = index_key {
                        result = if let Some(start) = start {
                            if let Some(end) = end {
                                if let Some(val) = parent_value.get(start as usize..end as usize) {
                                    Value::Array(val.to_vec())
                                } else {
                                    Value::Null
                                }
                            } else if let Some(val) = parent_value.get(start as usize..) {
                                Value::Array(val.to_vec())
                            } else {
                                Value::Null
                            }
                        } else if let Some(end) = end {
                            if let Some(val) = parent_value.get(..end as usize) {
                                Value::Array(val.to_vec())
                            } else {
                                Value::Null
                            }
                        } else {
                            Value::Array(parent_value)
                        };
                    } else {
                        return Err(Spanned::new(Error::BadArrayIndex(
                            index_key.type_name().to_string(),
                        ))
                        .with_span(index_span)
                        .with_file_path(file_path));
                    }
                } else if let Value::Object(mut parent_value) = parent_value {
                    if let Value::String(index_key) = index_key {
                        result = match parent_value.remove(&index_key) {
                            Some(v) => v,
                            None => Value::Null,
                        };
                    } else {
                        return Err(Spanned::new(Error::BadObjectIndex(
                            index_key.type_name().to_string(),
                        ))
                        .with_span(index_span)
                        .with_file_path(file_path));
                    }
                } else if let Value::String(mut parent_value) = parent_value {
                    if let Value::Number(index_key) = index_key {
                        if index_key.is_sign_positive() && index_key.fract() == 0.0 {
                            result = if (index_key as usize) < parent_value.len() {
                                // It is useful to use remove here to avoid cloning the value
                                // because we are sure that if the value comes from the Context it
                                // was already cloned before
                                Value::String(parent_value.remove(index_key as usize).to_string())
                            } else {
                                Value::Null
                            };
                        } else {
                            return Err(Spanned::new(Error::BadArrayIndex(
                                "a decimal number".to_string(),
                            ))
                            .with_span(index_span)
                            .with_file_path(file_path));
                        }
                    } else if let Value::Range(start, end) = index_key {
                        result = if let Some(start) = start {
                            if let Some(end) = end {
                                if let Some(val) = parent_value.get(start as usize..end as usize) {
                                    Value::String(val.to_string())
                                } else {
                                    Value::Null
                                }
                            } else if let Some(val) = parent_value.get(start as usize..) {
                                Value::String(val.to_string())
                            } else {
                                Value::Null
                            }
                        } else if let Some(end) = end {
                            if let Some(val) = parent_value.get(..end as usize) {
                                Value::String(val.to_string())
                            } else {
                                Value::Null
                            }
                        } else {
                            Value::String(parent_value)
                        };
                    } else {
                        return Err(Spanned::new(Error::BadStringIndex(
                            index_key.type_name().to_string(),
                        ))
                        .with_span(index_span)
                        .with_file_path(file_path));
                    }
                } else if parent_value == Value::Null {
                    return Err(Spanned::new(Error::CannotIndexNull)
                        .with_span(parent_span)
                        .with_file_path(file_path));
                } else {
                    return Err(Spanned::new(Error::ValueCannotBeIndexed(
                        parent_value.type_name().to_string(),
                    ))
                    .with_span(parent_span)
                    .with_file_path(file_path));
                }
            }
        }
    }

    Ok(result)
}
