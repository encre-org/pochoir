pub fn escape(val: &mut String) {
    let replacements = val
        .char_indices()
        .filter_map(|(i, ch)| match ch {
            '>' => Some((i, "&gt;")),
            '<' => Some((i, "&lt;")),
            '&' => Some((i, "&amp;")),
            '\'' => Some((i, "&#39;")),
            '"' => Some((i, "&quot;")),
            _ => None,
        })
        .collect::<Vec<(usize, &str)>>();

    let mut offset = 0;
    for (i, new_str) in replacements {
        // Only one byte is replaced so we don't care about the UTF8 codepoint boundaries
        val.replace_range(offset + i..=offset + i, new_str);
        offset += new_str.len() - 1;
    }
}
