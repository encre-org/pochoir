use std::borrow::Cow;

mod html;

/// Define the text characters escaped.
///
/// [`Escaping::Html`] is the default value.
#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub enum Escaping {
    /// No escaping is done.
    None,

    /// Problematic HTML characters are converted to HTML entities.
    ///
    /// - `>` will be replaced with `&gt;`
    /// - `<` will be replaced with `&lt;`
    /// - `&` will be replaced with `&amp;`
    /// - `'` will be replaced with `&#39;`
    /// - `"` will be replaced with `&quot;`
    ///
    /// [OWASP reference](https://cheatsheetseries.owasp.org/cheatsheets/Cross_Site_Scripting_Prevention_Cheat_Sheet.html#output-encoding-for-html-contexts)
    Html,
}

impl Escaping {
    /// Escape a piece of text using the specified [`Escaping`] variant.
    pub fn escape<'a, T: Into<Cow<'a, str>>>(&self, val: T) -> Cow<'a, str> {
        match self {
            Self::Html => {
                let mut buffer = val.into().to_string();
                html::escape(&mut buffer);
                Cow::Owned(buffer)
            }
            Self::None => val.into(),
        }
    }
}

impl Default for Escaping {
    fn default() -> Self {
        Self::Html
    }
}
