#![no_main]

use libfuzzer_sys::fuzz_target;
use pochoir::{Context, StaticMapProvider};
use std::borrow::Cow;

fuzz_target!(|data: &[u8]| {
    if let Ok(s) = std::str::from_utf8(data) {
        let mut provider = StaticMapProvider::new();
        provider.insert("template.html", Cow::Borrowed(s), None);

        if let Ok(compiled) = provider.compile("template.html", &mut Context::new()) {
            let _ = compiled;
        }
    }
});
