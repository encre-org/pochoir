//! Example of a page displaying several products separated into several reusable components.
use pochoir::{
    lang::{object, Context},
    providers::StaticMapProvider,
};

const MY_BUTTON_SOURCE: &str = r#"<button class="button button-primary">Buy</button>"#;
const MY_CARD_SOURCE: &str = r#"<div class="card">
  <div class="card-header">
    {{ product.name }} (article ID: {{ product.id }})
  </div>
  <div class="card-body">
    <my-button />
  </div>
</div>"#;
const DASHBORD_SOURCE: &str = r#"<header>
    <h1>Dashboard</h1>
  </header>

  <main>
    {% for product in products %}
    <my-card product="{{ product }}" />
    {% endfor %}
  </main>

  <footer>
  Copyright © 2023 John Doe
  </footer>
"#;

pub fn main() {
    let provider = StaticMapProvider::new()
        .with_template("my-button", MY_BUTTON_SOURCE, None)
        .with_template("my-card", MY_CARD_SOURCE, None)
        .with_template("dashbord", DASHBORD_SOURCE, None);

    let mut context = Context::from_object(object! {
        // In a real application, the products would be fetched from a database or
        // deserialized from a JSON file, but here they are just stored in memory
        "products" => [object! {
            "id" => "a61ab457-e16e-4e0b-82db-5ad3f623378a",
            "name" => "Elegant Fresh Table",
        }, object! {
            "id" => "118723ec-b7e7-4f9d-9066-3d8480c518ef",
            "name" => "Handmade Rubber Gloves",
        }, object! {
            "id" => "1430d157-68a7-4c67-9030-bb432931bc94",
            "name" => "Plastic Chair",
        }],
    });

    println!(
        "{}",
        provider
            .compile("dashbord", &mut context)
            .expect("compilation should not fail")
    );
}
