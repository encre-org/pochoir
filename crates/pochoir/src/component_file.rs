use std::{borrow::Cow, path::Path};

/// A structure representing an in-memory file with the path to the file on the disk on its data.
#[derive(Debug, PartialEq, Eq, Clone)]
pub struct ComponentFile<'a, 'b> {
    /// The path to the file on the disk.
    pub path: Cow<'a, Path>,

    /// The data of the file as a string (all components must be strings).
    pub data: Cow<'b, str>,
}

impl<'a, 'b> ComponentFile<'a, 'b> {
    /// Create a new [`ComponentFile`] from a path and some data.
    pub fn new<A: Into<Cow<'a, Path>>, B: Into<Cow<'b, str>>>(path: A, data: B) -> Self {
        Self {
            path: path.into(),
            data: data.into(),
        }
    }

    /// Create a new [`ComponentFile`] from some data. The path will be `inline`.
    pub fn new_inline<T: Into<Cow<'b, str>>>(data: T) -> Self {
        Self {
            path: Cow::Borrowed(Path::new("inline")),
            data: data.into(),
        }
    }
}
