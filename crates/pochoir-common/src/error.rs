use thiserror::Error;

use crate::Spanned;

pub type Result<T> = std::result::Result<T, Spanned<Error>>;

#[derive(Error, Debug, PartialEq, Eq, Clone)]
pub enum Error {
    #[error("unexpected end of input")]
    UnexpectedEoi,

    #[error("expected {expected:?}, found end of input")]
    ExpectedFoundEoi { expected: String },

    #[error("expected {expected:?}, found {found:?}")]
    UnexpectedInput { expected: String, found: String },
}
